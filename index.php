<!DOCTYPE html>
<html>
<head>
    <meta charset=utf-8>
    <title>Atom-M CMS</title>
    <link href="css/plusstrap.min.css" rel="stylesheet">
    <link href="css/style.css?4" rel="stylesheet">
    <meta name="viewport" content="width=940px">
    <link rel="icon" href="favicon.ico" type="image/x-icon" />
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />

    <script src='js/jquery.js' type='text/javascript'></script>

    <script>
    $(window).scroll(function(e){
        if ($(window).scrollLeft()!=0) {
            $(window).scrollLeft(0);
        }

        var scrolled = $(window).scrollTop();
        var h = $(window).height();

        $('body').css('background-position','0 '+h+'px');
        $('.screen1').css('background-position','0 '+((-1)*scrolled*.1)+'px');
        $('.masthead .container').css('opacity',(h-scrolled)/h);

        x2 = h-scrolled;
        if (x2<0) {
            x2=0;
            $('.screen2 .row-fluid').css('top',(scrolled-h)*.2+'px');
            $('.screen2').css('opacity',(h*2-scrolled)/h*2);
        } else {
            $('.screen2 .row-fluid').css('top','0px');
        }
        $('.screen2 .span6:first-child').css('right',x2+'px');
        $('.screen2 .span6:last-child').css('left',x2+'px');

        x3 = h*2-scrolled;
        if (x3<0) {
            x3=0;
            $('.screen3 .row-fluid').css('top',(scrolled-h*2)*.2+'px');
            $('.screen3').css('opacity',(h*3-scrolled)/h*3);
        } else {
            $('.screen3 .row-fluid').css('top','0px');
        }
        $('.screen3 .span6:first-child').css('right',x3+'px');
        $('.screen3 .span6:last-child').css('left',x3+'px');

        x4 = h*3-scrolled;
        if (x4<0) {
            x4=0;
            $('.screen4').css('top',(scrolled-h*3)*.2+'px');
            $('.screen4').css('opacity',(h*4-scrolled)/h*4);
        } else {
            $('.screen4').css('top','0px');
        }





        $('.switchscreen .top').addClass('noactive').removeClass('active');
        $('.switchscreen .reviews').addClass('noactive').removeClass('active');
        $('.switchscreen .advantages').addClass('noactive').removeClass('active');
        $('.switchscreen .install').addClass('noactive').removeClass('active');
        $('.switchscreen .info').addClass('noactive').removeClass('active');
        if (scrolled<h) {
            $('.switchscreen .top').removeClass('noactive').addClass('active');
        } else if (scrolled<h*2) {
            $('.switchscreen .reviews').removeClass('noactive').addClass('active');
        } else if (scrolled<h*3) {
            $('.switchscreen .advantages').removeClass('noactive').addClass('active');
        } else if (scrolled<h*4) {
            $('.switchscreen .install').removeClass('noactive').addClass('active');
        } else {
            $('.switchscreen .info').removeClass('noactive').addClass('active');
        }
    });

    function ascroll(to){
        height = 0;
        to = $(to).attr('href');
        if (to == '#reviews') {
            height = $(window).height();
        } else if (to == '#advantages') {
            height = $(window).height()*2;
        } else if (to == '#install') {
            height = $(window).height()*3;
        } else if (to == '#info') {
            height = $(window).height()*4;
        }
        $("html,body").animate({"scrollTop":height},1000);
    }
    </script>
</head>
<body>


    <div class="navbar navbar-inverse navbar-fixed-top">
        <div class="navbar-inner">
            <div class="container">
                <div class="nav-collapse collapse pull-right">
                    <ul class="nav">
                        <li><a href="http://demo.atom-m.net/">Демо</a></li>
                        <li><a href="http://dev.atom-m.net/forum">Форум</a></li>
                        <li><a href="https://bitbucket.org/atom-m/cms/">Репозиторий</a></li>
                        <li><a href="https://bitbucket.org/atom-m/cms/wiki/">Wiki</a></li>
                    </ul>
                </div>
                <div class="nav-collapse collapse switchscreen">
                    <ul class="nav">
                        <li class="active top"><a href="#top" onclick="ascroll(this); return false">Atom-M CMS</a></li>
                        <li class="reviews"><a href="#reviews" onclick="ascroll(this); return false">Отзывы</a></li>
                        <li class="advantages"><a href="#advantages" onclick="ascroll(this); return false">Преимущества</a></li>
                        <li class="install"><a href="#install" onclick="ascroll(this); return false">Установка</a></li>
                        <li class="info"><a href="#info" onclick="ascroll(this); return false">Инфо</a></li>
                    </ul>
                </div> 
            </div>
        </div>
    </div>





    <div class="screen screen1 masthead" id="top">
        <div class="container">
            <div class="row-fluid">

                <div class="span12">
                    <div class="center">
                        <div class="logo"></div>
                        <div class="text"><b>Atom-M CMS</b> - это мощная, хорошо защищённая, не требовательная к ресурсам система управления сайтом, предназначенная для создания как сайта-визитки, так и мультимедийного портала с элементами социальной сети.</div>
                        <br><br>
                        <a class="btn btn-primary btn-large" href="#install" onclick="ascroll(this); return false">Скачать сейчас *</a>
                        <br><br><br><br>
                        * скачать бесплатно без регистрации без смс на высокой скорости<!-- SEO 80 lvl -->
                    </div>
                </div>

            </div>
        </div>
    </div>


    <div class="screen screen2" id="reviews">
        <div class="data">
            <div class="container">
                <div class="row-fluid">
                    <div class="span12">
                        <div class="block center">
                            <h2>О нас говорят</h2>
                        </div>
                    </div>
                </div>
                <div class="row-fluid">

                    <div class="span6 list">
                        <div class="block">
                            <p>На своем продолжительном жизненном  пути в интернете я повидал не мало CMS, главным критериям для меня была простота использования и возможность создания своего собственного шаблона.
                                AtomM(X) в данном плане меня полностью устраивает, адаптировать (верстать) шаблоны под данный движок одно удовольствие нету лишних меток, все делается буквально на лету.
                                Немаловажным фактором, который меня привлек в данной CMS это встроенный модуль форум.
                                Сколько вы видели бесплатных движков со встроенным форумом? Таких можно пересчитать по пальцам.
                                Еще хотелось бы сказать, что движок постоянно эволюционирует, постоянно добавляются новые функции, команда сайта старается из-за всех сил и радует пользователей обновлениями каждый день.</p>
                            <p class="whatname">Андрей Mishka Майоров</p>
                        </div>

                        <div class="block">
                            <p>Atom-M - одна из самых удобных систем управления контентом, в которых мне приходилось работать.
                                Доступ к разделам, модулям, страницам - все на виду, юзабилити проработано до мелочей.
                                Добавить новость, статью или иной текст на сайт можно буквально парой кликов мыши.
                                Надеюсь, Atom-M получит большое распространение и дальнейшее развитие.
                                Отдельно благодарю Вас за своевременную профессиональную высококачественную техническую поддержку.</p>
                            <p class="whatname">Александр -=DESTROY=- Дестроев</p>
                        </div>
                    </div>

                    <div class="span6">




                        <div class="soo">
                            <div class="we">Мы в соцсетях:</div>
                            <div class="icons">
                                <a href="https://twitter.com/AtomM_CMS" target="_blank" class="twitter"></a>
                                <a href="https://plus.google.com/+AtomMnetCMS" target="_blank" class="gplus"></a>
                                <a href="https://vk.com/atom_m" target="_blank" class="vk"></a>
                            </div>
                            <div style="clear:both"></div>
                        </div>




                        <div class="tweetblock">
                            <a class="twitter-timeline" href="https://twitter.com/AtomM_CMS"  data-widget-id="419906981332144128">Твиты пользователя @AtomM_CMS</a>
                            <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>















    <div class="screen screen3" id="advantages">
        <div class="data">
            <div class="container">
                <div class="row-fluid">
                    <div class="span12">
                        <div class="block center">
                            <h2>Преимущества</h2>
                        </div>
                    </div>
                </div>
                <div class="row-fluid">

                    <div class="span6">
                        <div class="block">
                            <h4>Новейшие технологии</h4>
                            <p>Atom-M CMS построена с использованием передовых технологий, таких как <i>PHP5, PDO, HTML5, jQuery, aJax, ООП</i> и т.д.<br>
                            Для удобства подготовки материалов по-умолчанию добавлен визуальный редактор BB кодов <i>WysiBB</i>.<br>
                            В движок интегрирован ЧПУ (адреса понятные человеку), который вы сами сможете конфигурировать под ваш вкус и ваш проект.<br>
                            Подключен лёгкий чат, полностью построенный на технологии <i>aJax</i>.</p>
                        </div>
                        <div class="block">
                            <h4>Шаблоны</h4>
                            <p>Шаблоны представляют собой обычные html странички, на которых вызываются метки. В шаблонах PHP код не обрабатывается, но результат его работы можно вывести через сниппет.<br>
                            Благодаря этому значительно повышаются безопасность и простота создания шаблонов.<br>
                            Темизации сайта уделяется больше всего внимания, ведь это очень важный фактор в создании сайта.<br>
                            В движок встроен производительный шаблонизатор, который даже при выключенном кэше гораздо быстрее популярного <i>Twig</i>, а при кэшировании скорость повышается в сотни раз.</p>
                        </div>
                    </div>
                    <div class="span6">
                        <div class="block">
                            <h4>Настройка SEO и разграничение прав</h4>
                            <p>Благодаря настройке индексации ссылок, выборочному скрытию контента от поисковиков, тегов к материалам и т.п. предоставляется множество рычагов управления SEO составляющей сайта.<br>
                            Внедрено повсеместное использование <i>ASL</i> (Системы разграничения прав доступа).</p>
                        </div>
                        <div class="block">
                            <h4>Расширение возможностей</h4>
                            <p>CMS имеет систему <i>сниппетов</i>, выполняющих php. С помощью них можно создать любой "глобальный блок" любого содержания.<br>
                            Помимо сниппетов используется очень мощная система <i>плагинов</i>, работающих через хуки.<br>
                            Всё это дополняют функциональные <i>модули</i>: Новости, Статьи, Загрузки, Фото-каталог, Чат, Форум, Поиск, Пользователи.</p>
                        </div>
                        <div class="block">
                            <h4 class="active">Мы открыты!</h4>
                            <p style="display:block">Сообщество Atom-M CMS открыто для общения, помощи и обмена идеями. Кроме того, вы можете и сами принимать участие в разработке, <a href="https://bitbucket.org/atom-m/cms/wiki/Помощь_в_развитии_проекта">любым из способов</a>.</p>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>















    <div class="screen screen4" id="install">
        <div class="data">
            <div class="container">
                <div class="row-fluid">
                    <div class="span12">
                        <div class="block center">
                            <h2>Установка</h2>
                        </div>
                    </div>
                </div>
                <div class="row-fluid" >

                    <div class="span8">
                        <div class="block">
                            <p style="padding-top: 5px"><b>1</b>. Получите место на хостинге (или установите веб-сервер на свой компьютер).<br><br>
                                <b>2</b>. Распакуйте все файлы архива с CMS на хостинг<br><br>
                                <b>3</b>. Выставьте права доступа 777 на следующие файл и папки рекурсивно:
                                    <div style="padding-left: 30px"><i>
                                    /data/<br>
                                    /plugins/<br>
                                    /sys/logs/<br>
                                    /sys/cache/<br>
                                    /sys/tmp/<br>
                                    /sys/settings/<br>
                                    /templates/<br>
                                    /sitemap.xml<br>
                                    /robots.txt</i></div><br>

                                <b>Рекурсивно</b> - это значит включая все подпапки и файлы. Это нужно выбрать при установке прав доступа в вашем файловом менеджере.<br><br>
                                <b>4</b>. После этого перейдите из браузера на свой сайт и следуйте инструкции.</p>
                        </div>
                    </div>

                    <div class="span4">
                        <div class="block w200 center">
                            <br>
                            <a class="btn btn-warning btn-large" href="https://bitbucket.org/atom-m/cms/get/current.zip">Последняя сборка</a><br>
                            <hr>
                            <a class="btn btn-success btn-large" href="https://bitbucket.org/atom-m/cms/downloads/atom-m-3-b65fc97.zip">Стабильная версия (v3)</a>
                            <hr>
                            <a class="btn btn-primary btn-large" href="https://bitbucket.org/atom-m/cms/downloads">все версии</a><br><br>
                        </div>
                        <div class="block w200">
                            <p style="padding-top: 5px">
                                <b>Atom-M</b> развивается силами сообщества - таких же людей, как вы, и защищён <i>свободной лицензией MIT</i>.
                            </p>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>




    <div class="screen screen5" id="info">
        <div class="data">
            <div class="container">
                <div class="row-fluid" >

                    <div class="span8">
                        <div class="block history">
                            <h3>О нас</h3>
                            <p>
                                Начну с небольшого лирического отступления. Как то раз <i>Андрей Drunya Брыкин</i> захотел создать свой сайт. Открыл форум на одном из сервисов, потом сайт на юкоз... И тогда его посетила мысль, что сайт не его: он принадлежит юкозу.
                                Почитав немного статей о раскрутке он, к тому же, понял, что такой сайт нереально серьезно раскрутить. Тогда он собрался с мыслями и начал искать готовые CMS, но вскоре был просто поражен тем, что действительно удобного и в тоже время гибкого, решения так и не нашел.
                            </p>
                            <p>
                                С тех пор он начал делать свою CMS. Сначала только для своего сайта, потом выложил в открытый доступ.
                                Время шло, движок развивался. Было много критики, но ещё больше одобрения и помощи. Множество людей внесли свой вклад в развитие движка.
                            </p>
                            <p>
                                Через какое то время разработка переместилась в Git, <i>Александр modos189 Данилов</i> и <i>Александр Wasja Вереник</i> создали ответвления, в которых меняли движок под своё видение.
                                С этих пор можно начинать отсчёт существования Atom-M, ведь на основе форка <i>modos189</i> был создан общий репозиторий, в который может добавить своё изменение любой желающий, если, конечно, никто не будет против.
                            </p>
                            <p>
                                Сейчас в разработчиках <b>Atom-M</b> также <i>Борис MrBoriska Лапин</i>, делающий из всего дизайна CMS конфетку.
                                Но нельзя не упомянуть и таких людей как <i>Александр -=DESTROY=- Дестроев</i>, который помог найти множество багов, ныне уже исправленных, и <i>Андрей Mishka Майоров</i>, улучшающий плагины.
                            </p>
                            <p>
                                Присоединяйся и ты! Да, ты, читающий это сообщение!
                            </p>
                        </div>
                    </div>

                    <div class="span4 center">
                        <div class="block w200">
                            <p style="padding-top: 5px">
                                <h5>Также не откажемся от финансовой помощи. Нам ведь надо покупать хостинги, рекламу, кофе, ну вы поняли.</h5>
                                <br>
                                <h4>modos189</h4>
                                <h6>представитель Atom-M</h6>
                                Z170048148370<br>
                                R312807696558<br>
                                E103018815543<br>
                                Яндекс.Деньги 410011752911192
                                <br><br><br>
                                <h4>Drunya</h4>
                                <h6>представитель Atom-X</h6>
                                Z320956365350<br>
                                U237030902343<br>
                                R405239497933
                            </p>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <script src="js/plusstrap.min.js"></script>
    <!-- Yandex.Metrika counter -->
    <script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter25331210 = new Ya.Metrika({id:25331210,
                        clickmap:true,
                        trackLinks:true,
                        accurateTrackBounce:true,
                        ut:"noindex"});
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
    </script>
    <noscript><div><img src="//mc.yandex.ru/watch/25331210?ut=noindex" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
    <!-- /Yandex.Metrika counter -->
</body>
</html>